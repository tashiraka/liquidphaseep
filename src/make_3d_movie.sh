#!/bin/bash


if [ -d png ]; then
    rm -r png
fi
    
if [ ! -d png ]; then
    mkdir png
fi

loop_size=1000
hdf=test_70_500_$loop_size.hdf5

for i in `seq 950 1000`; do
    PATH=$HOME/src/pymol/bin:$PATH LIBRARY_PATH=$HOME/src/pymol/lib:$LIBRARY_PATH LD_LIBRARY_PATH=$HOME/src/pymol/lib:$LD_LIBRARY_PATH PYTHONPATH=$HOME/src/pymol/lib/python2.7/site-packages:$PYTHONPATH python make_3d_movie.py $hdf $i 1216 100 $loop_size png
done

LD_LIBRARY_PATH=/lib64:/lib:/usr/local/lib64:/usr/local/lib:/usr/lib64:/usr/lib:LD_LIBRARY_PATH convert -layers optimize -dispose previous -verbose png/*.png test_70_500_${loop_size}.gif
