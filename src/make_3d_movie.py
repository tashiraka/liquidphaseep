#!/home/yuichi/bin/python2.7

import sys, os, h5py, pymol, subprocess, tempfile


args = sys.argv
in_file = args[1]
sample_num = int(args[2])
polymer_length = int(args[3])
num_x = int(args[4])
ep_length = int(args[5])
out_dir = args[6]

sample_start = sample_num
sample_end = sample_num + 1

# Pymol uses 1-origin index and closed interval [start, end]
polymer_start = 1
polymer_end = polymer_length
x_start = polymer_end + 1
x_end = polymer_end + num_x


margin_length = 100
p_length = 1
#ep_length = 500 #67
e_length = 15
e_positions = range(margin_length + 1,
                    margin_length + e_length + 1)
p_positions = [margin_length + e_length + ep_length + 1]

pymol.pymol_argv = ['pymol','-qc']
cmd = pymol.cmd
pymol_obj_name = "mov"


c_radius = 1
x_radius = 1


h5file = h5py.File(in_file, "r")


for i in range(sample_start, sample_end):
    tmp_file = tempfile.NamedTemporaryFile(suffix='.xyz')
    pymol.finish_launching()
    
    print i
    positions = h5file[str(i)].value

    for z, position in enumerate(positions):
        xyz_fmt_line = " ".join(["UNK"] + [str(x) for x in position]) + "\n"
        tmp_file.write(xyz_fmt_line)

    tmp_file.flush()
    cmd.load(tmp_file.name, pymol_obj_name, 1, format="xyz")
    cmd.hide("all")

    for j in range(polymer_start, polymer_end - 1):
        cmd.bond("index " + str(j), "index " + str(j + 1))

    cmd.alter("all", "vdw=1")
    cmd.set("stick_radius", c_radius * 0.25)
    cmd.set("stick_transparency", 0.50)
    cmd.show("sticks", "i. " + str(polymer_start) + "-" + str(polymer_end))

    cmd.set("sphere_scale", x_radius)

    for j in e_positions:
        cmd.color("cyan", "i. " + str(j) + "-" + str(j))
        cmd.show("spheres", "i. " + str(j) + "-" + str(j))

    for j in p_positions:
        cmd.color("yellow", "i. " + str(j) + "-" + str(j))
        cmd.show("spheres", "i. " + str(j) + "-" + str(j))

    for j in range(x_start, x_end):
        cmd.color("red", "i. " + str(j) + "-" + str(j))
        cmd.show("spheres", "i. " + str(j) + "-" + str(j))
        cmd.set("sphere_transparency", 0.40, "i. " + str(j) + "-" + str(j))

    cmd.set("ray_opaque_background", "on")
    cmd.png(out_dir + '/' + '%05d' % i + '.png')

    cmd.quit()
