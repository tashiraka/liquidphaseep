#!/bin/bash

#
# Dir pathes
#
data_path=/work2/yuichi/Project_EP/loop_extrusion_through_medaka_development/data
plugin_dir=../externals/OpenMM/installed/lib/plugins

            
# Parameters
num_xs=(10 50 100)
epsilons=(50 55 60 65 70) #(70 85 100)
epsilon2s=(500) #(100 350 500)
loop_lengthes=(500) #(100 500 1000)
sample_num=50

for num_x in ${num_xs[@]}; do
    for epsilon in ${epsilons[@]}; do
        for epsilon2 in ${epsilon2s[@]}; do
            for loop_length in ${loop_lengthes[@]}; do

                id=${num_x}_${epsilon}_${epsilon2}_${loop_length}
                
                hdf=test_${id}.hdf5
                png=test_${id}.png
                
                set -x
                time LD_LIBRARY_PATH=../externals/OpenMM/installed/lib:../externals/OpenMM/installed/lib/plugins:../externals/HDF5/installed/lib:$LD_LIBRARY_PATH ./simulate_single_loop $hdf $plugin_dir $num_x $epsilon $epsilon2 $loop_length
                python trace_ep_distance.py $hdf $sample_num $loop_length $png
                set +x
            done
        done
    done
done
