/*
 * 1.5 kbp sna shadow enhancer
 * 100 bp sna promoter
 * Two gene promoters share a enahncer.
 * The two promoters are symmetrically located 7.6 kbp apart from the enhancer. 
 */

#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <algorithm>
#include <memory>
#include <iomanip>
#include <map>
#include <cmath>
#include "H5Cpp.h"
#include "OpenMM.h"


namespace EPSim
{
  double rand_prob() {
    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_real_distribution<> dist(0, 1);
    return dist(mt);
  }


  
  int rand_int(int start, int end) {
    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_int_distribution<> dist(start, end);
    return dist(mt);
  }


  
  std::vector<double> accumulate(std::vector<double> arr)
  {
    std::vector<double> cumulative_arr(arr.size(), 0);
    
    if (arr.size() == 0) return cumulative_arr;
    
    cumulative_arr[0] = arr[0];
    for (size_t i=1; i<arr.size(); i++)
      cumulative_arr[i] = cumulative_arr[i - 1] + arr[i];
    
    return cumulative_arr;
  }


  
  int sample_from_cumulative_prob(const std::vector<double>& cumulative_prob)
  {
    auto it = std::upper_bound(cumulative_prob.begin(), cumulative_prob.end(), rand_prob());
    auto pos = it - cumulative_prob.begin();
    return pos;
  }


  /***
   * !!!! ALL values have some units. !!!!
   * The default units are OpenMM are nanometers, picoseconds, amu, mol, 
   * kiloJoule and Kelvin.
   * Friction coefficient is in inverse picoseconds.
   * The potential in the reference paper is in [kB*T], but for OpenMM, in [kJ/mol].
   * The conversion is [kB*T] = kB * T * 1e-3 * avogadro_number [kJ/mol].
   ***/  
  class Parameters
  {
  public:
    /***************************************************************
     * Files
     ***************************************************************/
    const std::string out_file;   // HDF5
    const std::string plugin_dir; // path to OpenMM plugins

    
    /***************************************************************
     * System
     ***************************************************************/
    const std::string platform_name = "CUDA";
    const std::string precision = "mixed";
    const std::string integrator_name = "langevin";
    const double temperature = 300;            // [K]
    const double kB = 1.380658e-23;            // [J/K]    openmm/reference/SimTKOpenMMRealType.h
    const double avogadro = 6.0221367e23;      // [mol^-1] openmm/reference/SimTKOpenMMRealType.h
    const double kBT_to_kJPerMol = kB * temperature * avogadro * 1e-3;
    const double friction_coeff = 0.01;        // [ps^-1]
    const double box_size;                     // [nm] a side length of box

    
    /***************************************************************
     * Dynamics
     ***************************************************************/
    const double time_step = 0.001;             // [ps]
    const int relax_steps_per_cycle = 10000;
    const int relax_cycles = 100;
    const int non_saved_steps = 0;
    const int saved_steps = 1;
    const int total_saved_steps = 1;

    
    /***************************************************************
     * Liquid-phase constructer X
     ***************************************************************/
    const double x_radius = 1; // [nm]
    const double x_mass = 1; // [amu]
    const double x_volume = 4.0 / 3.0 * M_PI * x_radius * x_radius * x_radius;
    const double x_density;        // [nm^-3]
    const int num_x;
    
    /***************************************************************
     * Potentials
     ***************************************************************/
    // LJ potentials
    const double lj_epsilon;    
    const double lj_sigma = x_radius * 2;
    const double lj_cutoff = 4 * lj_sigma;
    const double lj_switch = 3.5 * lj_sigma;
    
    
    Parameters(const std::string& of_path, const std::string& p_path,
               const double b_s, const double x_d, const double e)
      : out_file(of_path), plugin_dir(p_path), box_size(b_s), x_density(x_d),
        num_x(x_density * box_size * box_size * box_size / x_volume), lj_epsilon(e)
    {
      std::cout << num_x << std::endl;
    }
  };

  
  class Simulator_3D
  {
    const std::shared_ptr<const EPSim::Parameters> params;
    OpenMM::System* system = nullptr;
    OpenMM::Platform* platform = nullptr;
    OpenMM::LangevinIntegrator* integrator = nullptr;
    OpenMM::Context* context = nullptr;
    
    // The Forces must not be deleted by myself. System object delete them.
    std::vector<OpenMM::CustomNonbondedForce*> lj_forces;

    // Particle indices in the System
    std::vector<int> x_idx;

    // Return random positions of X
    // because the nucleation seems to be avoided in the initial positoins.
    std::vector<OpenMM::Vec3> x_random_positions()
    {
      // Firstly set the X positions in the [1, virtual_box_size]^3 lattice
      int virtual_box_size = params->box_size / (2 * params->x_radius);
      std::vector<std::array<int, 3>> virtual_positions;

      // put X on the lattice randomly
      std::vector<std::array<int, 3>> lottery_box;
      for (int i=0; i<virtual_box_size; i++)
        for (int j=0; j<virtual_box_size; j++)
          for (int k=0; k<virtual_box_size; k++)
            lottery_box.push_back(std::array<int, 3>{{i, j, k}});

      for (int i=0; i < params->num_x; i++) {
        if (lottery_box.size() == 0) {
          std::cerr << "X cannot be initialized because of small space." << std::endl;
          std::exit(1);
        }

        auto lot_idx = rand_int(0, lottery_box.size() - 1);
        auto pos = lottery_box[lot_idx];
        lottery_box.erase(lottery_box.begin() + lot_idx);
        
        virtual_positions.push_back(std::array<int, 3>{{pos[0], pos[1], pos[2]}});
      }
      

      // Reverse-transform
      std::vector<OpenMM::Vec3> positions(params->num_x);
      for (size_t i=0; i < positions.size(); i++)
        for (int j=0; j < 3; j++) 
          positions[i][j] = virtual_positions[i][j] * (2 * params->x_radius);

      return positions;
    }


    // Return the particles positons with the order: polymer postitions -> X positions
    std::vector<OpenMM::Vec3> random_positions()
    {
      return x_random_positions();
    }

    
    // Sampled from Maxwell distribution
    std::vector<OpenMM::Vec3> random_velocities()
    {
      std::vector<OpenMM::Vec3> velocities;
      std::random_device rnd;
      std::mt19937 mt(rnd());

      std::vector<double> masses{params->x_mass};

      std::vector<double> std_devs(masses.size());
      for (size_t i = 0; i < masses.size(); i++)
        std_devs[i] = sqrt(params->kB * params->temperature / masses[i]) * 1e-3; // [nm / ps]

      std::vector<std::normal_distribution<double>> normals;
      for (size_t i = 0; i < masses.size(); i++)
        normals.push_back(std::normal_distribution<double>(0, std_devs[i]));

      std::vector<int> nums = {params->num_x};
      for (size_t i=0; i < masses.size(); i++)
        for (int j=0; j < nums[i]; j++)
          velocities.push_back(OpenMM::Vec3(normals[i](mt), normals[i](mt), normals[i](mt)));
      
      return velocities;
    }

    
  public:
    ~Simulator_3D()
    {
      delete context;
      delete system;
      delete platform;
      delete integrator;
    }

    
    Simulator_3D(const std::shared_ptr<const EPSim::Parameters>& p)
      : params(p)
    {
      
      // System
      system = new OpenMM::System();
      system->setDefaultPeriodicBoxVectors({p->box_size, 0, 0},
                                           {0, p->box_size, 0},
                                           {0, 0, p->box_size});

      int particle_counter = 0;
      for (int i=0; i < p->num_x; i++) {
        system->addParticle(p->x_mass);
        x_idx.push_back(particle_counter);
        particle_counter++;
      }

      // Forces
      // Nonbonded attractive and repulsive forces (LJ potensial)
      if (p->lj_epsilon != 0) {
        const std::string lj_force_eq = "4 * epsilon * ((sigma/r)^12 - (sigma/r)^6);";

        // Set LJ force for X
        OpenMM::CustomNonbondedForce* lj_force = new OpenMM::CustomNonbondedForce(lj_force_eq);
        lj_force->addGlobalParameter("sigma", p->lj_sigma);
        lj_force->addGlobalParameter("epsilon", p->lj_epsilon);
        lj_force->setNonbondedMethod(lj_force->CutoffPeriodic); // Periodic boundary condition
        lj_force->setCutoffDistance(p->lj_cutoff);
        lj_force->setUseSwitchingFunction(true);
        lj_force->setSwitchingDistance(p->lj_switch);
      
        for (int k=0; k < system->getNumParticles(); k++)
          lj_force->addParticle();

        lj_forces.push_back(lj_force);
        system->addForce(lj_force);
      }
      
      // Platform
      OpenMM::Platform::loadPluginsFromDirectory(params->plugin_dir);

      // Accept CUDA and CPU
      auto platform_name_lower = p->platform_name;
      std::transform(platform_name_lower.begin(), platform_name_lower.end(),
                     platform_name_lower.begin(), ::tolower);
      
      if (platform_name_lower == "cuda") {
        platform = &OpenMM::Platform::getPlatformByName("CUDA");
        platform->setPropertyDefaultValue("CudaPrecision", p->precision);
      }
      else if (platform_name_lower == "cpu") {
        platform = &OpenMM::Platform::getPlatformByName("CPU");
      }
      else {
        std::cerr << "Error: Accepting only CUDA or CPU" << std::endl;
        std::exit(1);
      }

      
      // Integrator
      auto integrator_name_lower = p->integrator_name;
      std::transform(integrator_name_lower.begin(), integrator_name_lower.end(),
                     integrator_name_lower.begin(), ::tolower);

      // Accept only langevin
      if (integrator_name_lower == "langevin") {
        integrator = new OpenMM::LangevinIntegrator(p->temperature, p->friction_coeff,
                                                    p->time_step);
      }
      else {
        std::cerr << "Error: Accepting only langeivn" << std::endl;
        std::exit(1);
      }
    }


    void init()
    {
      context = new OpenMM::Context(*system, *integrator, *platform);
      context->setPositions(random_positions());
      context->setVelocities(random_velocities());

      auto state = get_state();
      std::cout << "Initial kinetics: " <<
        state.getKineticEnergy() / system->getNumParticles() / params->kBT_to_kJPerMol << std::endl;
      std::cout << "Initail potential: " <<
        state.getPotentialEnergy() / system->getNumParticles() / params->kBT_to_kJPerMol << std::endl;
    }

    
  public:
    void steps(int num_steps)
    {
      // No local energy minimization
      integrator->step(num_steps);
    }

    OpenMM::State get_state() const
    {
      return context->getState(OpenMM::State::Positions |
                               OpenMM::State::Velocities |
                               OpenMM::State::Forces |
                               OpenMM::State::Energy);
    }

    
    void print_energy()
    {
      auto state = get_state();
      std::cout << std::setw(5) << std::left << std::setfill('0') <<
        "Kinetics: " <<
        state.getKineticEnergy()  / system->getNumParticles() / params->kBT_to_kJPerMol <<
        "    Potential: " <<
        state.getPotentialEnergy() / system->getNumParticles() / params->kBT_to_kJPerMol;
    }
    
    
    std::vector<OpenMM::Vec3> get_positions() const
    {
      return context->getState(OpenMM::State::Positions).getPositions();
    }


    void save_positions(H5::H5File file, std::string data_name) const
    {
      auto positions = get_positions();
      
      // Create data set
      auto nrow = positions.size();
      auto ncol = 3;
      hsize_t data_dim[2]{hsize_t(nrow), hsize_t(ncol)};
      H5::DataSpace data_space(2, data_dim);
      auto data_set = file.createDataSet(data_name.c_str(), H5::PredType::NATIVE_DOUBLE, data_space);
      
      // For HDF5, the array must have a contiguous memory.
      // So, it cannot recognize a pointer array, e.g. std::vector and double**.
      double *data = new double[nrow * ncol];
      
      for (size_t i = 0; i < positions.size(); i++) {
        for (int j = 0; j < 3; j++) {
          data[i * ncol + j] = positions[i][j];
        }
      }

      data_set.write(data, H5::PredType::NATIVE_DOUBLE);
      delete data;
    }
  };

    class EP_simulator
  {
    const std::shared_ptr<const EPSim::Parameters> params;
    EPSim::Simulator_3D sim_3d;


    
  public:
    EP_simulator(const std::shared_ptr<const EPSim::Parameters> p)
      : params(p), sim_3d(p) {}
    
    void run()
    {
      H5::H5File h5_file(params->out_file.c_str(), H5F_ACC_TRUNC);

      // Initialization
      std::cout << "Initialization" << std::endl;
      sim_3d.init();

      // Relaxation
      std::cout << "Relaxation" << std::endl;
      for (int i=0; i < params->relax_cycles; i++) {
        sim_3d.steps(params->relax_steps_per_cycle);

        // Print the progress and energy
        std::cout << "Relaxation: " << (i + 1) * params->relax_steps_per_cycle << "/"
                  << params->relax_cycles * params->relax_steps_per_cycle << "    ";
        sim_3d.print_energy();
        std::cout << std::endl;
      }

      // Run until reaching total saved steps      
      int saved_steps_counter = 0;
      bool break_flag = false;
      
      while (saved_steps_counter < params->total_saved_steps) {
        for (int i=0; i < params->saved_steps; i++) {
          sim_3d.steps(1);
          sim_3d.save_positions(h5_file, std::to_string(saved_steps_counter));
          saved_steps_counter++;

          if (saved_steps_counter >= params->total_saved_steps) {
            break_flag = true;
            break;
          }
        }

        std::cout << "Saved: " << saved_steps_counter << "/"
                  << params->total_saved_steps << "    ";
        sim_3d.print_energy();
        std::cout << std::endl;

        if (!break_flag)
          sim_3d.steps(params->non_saved_steps);
      }
    }
  };
}


int main(int argc, char *argv[])
{
  const std::string out_file = argv[1];
  const std::string plugin_dir = argv[2];
  const int box_size = std::atof(argv[3]);
  const double x_density = std::atof(argv[4]);
  const double epsilon = std::atof(argv[5]);

  const std::shared_ptr<const EPSim::Parameters> params(new EPSim::Parameters(out_file, plugin_dir,
                                                                              box_size, x_density, epsilon));
  const std::unique_ptr<EPSim::EP_simulator> sim(new EPSim::EP_simulator(params));
  sim->run();
  
  return 0;
}


