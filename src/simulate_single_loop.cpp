/*
 * 1.5 kbp sna shadow enhancer
 * 100 bp sna promoter
 * Two gene promoters share a enahncer.
 * The two promoters are symmetrically located 7.6 kbp apart from the enhancer. 
 */

#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <algorithm>
#include <memory>
#include <iomanip>
#include <map>
#include <cmath>
#include <boost/range/irange.hpp>
#include "H5Cpp.h"
#include "OpenMM.h"


namespace EPSim
{
  double rand_prob() {
    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_real_distribution<> dist(0, 1);
    return dist(mt);
  }


  
  int rand_int(int start, int end) {
    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_int_distribution<> dist(start, end);
    return dist(mt);
  }


  
  std::vector<double> accumulate(std::vector<double> arr)
  {
    std::vector<double> cumulative_arr(arr.size(), 0);
    
    if (arr.size() == 0) return cumulative_arr;
    
    cumulative_arr[0] = arr[0];
    for (size_t i=1; i<arr.size(); i++)
      cumulative_arr[i] = cumulative_arr[i - 1] + arr[i];
    
    return cumulative_arr;
  }


  
  int sample_from_cumulative_prob(const std::vector<double>& cumulative_prob)
  {
    auto it = std::upper_bound(cumulative_prob.begin(), cumulative_prob.end(), rand_prob());
    auto pos = it - cumulative_prob.begin();
    return pos;
  }


  /***
   * !!!! ALL values have some units. !!!!
   * The default units are OpenMM are nanometers, picoseconds, amu, mol, 
   * kiloJoule and Kelvin.
   * Friction coefficient is in inverse picoseconds.
   * The potential in the reference paper is in [kB*T], but for OpenMM, in [kJ/mol].
   * The conversion is [kB*T] = kB * T * 1e-3 * avogadro_number [kJ/mol].
   ***/  
  class Parameters
  {
  public:
    /***************************************************************
     * Files
     ***************************************************************/
    const std::string out_file;   // HDF5
    const std::string plugin_dir; // path to OpenMM plugins

    
    /***************************************************************
     * Dynamics
     ***************************************************************/
    const double time_step = 0.001;//10.0;             // [ps]
    const int relax_steps_per_cycle = 1000;
    const int relax_cycles = 1000;
    const int non_saved_steps = 1000;
    const int saved_steps = 1;
    const int total_saved_steps = 50;

    
    /***************************************************************
     * Chromatin fiber C, Promoter P1 and P2, Enhancer E
     ***************************************************************/
    const double c_radius = 1; // [nm]
    const double c_mass = 1;//262000.0 / 2.0;     // [amu], 262 kDa per one nucleosome and 200 bp DNA
    const int c_monomer_bp = 100; // [bp]
    const int margin_length = 100;
    const int p_length = 1;
    const int ep_length;// = 67;
    const int e_length = 15;
    const int num_c_monomers
    = margin_length + p_length + ep_length + e_length + margin_length;
    std::vector<int> e_positions;
    std::vector<int> p_positions;
    
    /***************************************************************
     * Liquid-phase constructer X
     ***************************************************************/
    const double x_radius = 1;//c_radius * 0.1; // [nm]
    const double x_mass = 1;//c_mass * 0.1; // [amu]

    
    /***************************************************************
     * System
     ***************************************************************/
    const std::string platform_name = "CUDA";
    const std::string precision = "mixed";
    const std::string integrator_name = "langevin";
    const double temperature = 300;            // [K]
    const double kB = 1.380658e-23;            // [J/K]    openmm/reference/SimTKOpenMMRealType.h
    const double avogadro = 6.0221367e23;      // [mol^-1] openmm/reference/SimTKOpenMMRealType.h
    const double kBT_to_kJPerMol = kB * temperature * avogadro * 1e-3;
    const double friction_coeff = 0.01;        // [ps^-1]
    const double c_density = 0.001;
    //    const double x_density = 0.0004;        // [nm^-3]
    const double box_size = std::pow(4.0 / 3.0 * M_PI * c_radius * c_radius * c_radius
                                     * num_c_monomers / c_density,
                                     1.0 / 3.0);//110 * 1.5;*/// [nm] a side length of box
    const int num_x; // = 100;
    //= x_density * box_size * box_size * box_size / (4.0 / 3.0 * M_PI * x_radius * x_radius * x_radius);

    
    /***************************************************************
     * Potentials
     ***************************************************************/
    const double c_bond_natural_length = c_radius * 2;      // [nm]
    const double c_bond_k = 100 * kBT_to_kJPerMol;          // [kJ/mol/nm^2]
    const double c_natural_angle = 90;                 // [radian]
    const double c_angle_k = 0 * kBT_to_kJPerMol;         // [kJ/mol/nm^2]

    // LJ potentials
    // C:0, E:1, P:2, X:3
    const double e;// = 100;
    const double e2;
    const double s = c_radius * 2;
    const double s2 = x_radius + c_radius;
    const double s3 = x_radius * 2;
    const double cutoff = 4;
    const double switching1 = 0.99;
    const double switching2 = 3.5;
    std::vector<std::vector<double>> lj_epsilon_table = {{e, e, e, e},
                                                         {e, e, e, e2},
                                                         {e, e, e, e2},
                                                         {e, e2, e2, e}};
    
    std::vector<std::vector<double>> lj_sigma_table = {{s, s, s, s2},
                                                       {s, s, s, s2},
                                                       {s, s, s, s2},
                                                       {s2, s2, s2, s3}};

    std::vector<std::vector<double>> lj_cutoff_table = {{s, s, s, s2},
                                                        {s, s, s, cutoff * s2},
                                                        {s, s, s, cutoff * s2},
                                                        {s2, cutoff * s2, cutoff * s2, cutoff * s3}};
    
    std::vector<std::vector<double>> lj_switch_table = {{switching1 * s, switching1 * s, switching1 * s, switching1 * s2},
                                                        {switching1 * s, switching1 * s, switching1 * s, switching2 * s2},
                                                        {switching1 * s, switching1 * s, switching1 * s, switching2 * s2},
                                                        {switching1 * s2, switching2 * s2, switching2 * s2, switching2 * s3}};
    
    
    Parameters(const std::string& of_path, const std::string& p_path, const int n_x,
               const double epsilon, const double epsilon2, const int loop_length)
      : out_file(of_path), plugin_dir(p_path), num_x(n_x), e(epsilon), e2(epsilon2), ep_length(loop_length)
    {
      std::cout << "#C = " << num_c_monomers << std::endl;
      std::cout << "#X = " << num_x << std::endl;
      
      for (int i=0; i < e_length; i++)
        e_positions.push_back(margin_length +  i);

      for (int i=0; i < p_length; i++)
        p_positions.push_back(margin_length + e_length + ep_length + i);
    }
  };

  
  class Simulator_3D
  {
    const std::shared_ptr<const EPSim::Parameters> params;
    OpenMM::System* system = nullptr;
    OpenMM::Platform* platform = nullptr;
    OpenMM::LangevinIntegrator* integrator = nullptr;
    OpenMM::Context* context = nullptr;
    
    // The Forces must not be deleted by myself. System object delete them.
    OpenMM::HarmonicBondForce* bond_force = nullptr;
    OpenMM::HarmonicAngleForce* stiff_force = nullptr;
    std::vector<OpenMM::CustomNonbondedForce*> lj_forces;

    // Particle indices in the System
    std::vector<int> c_idx, e_idx, p_idx, x_idx;


    std::vector<OpenMM::Vec3> polymer_random_positions()
    {
      /***
       * Discritize box size for calculation.
       * Transform params->initial_bond_length to 1
       * For example, when box_size = 1 and initial_bonx_length = 0.3,
       * virtual_box_size = 3 and grids are 0, 1, 2 (= 0, 0,3, 0,6, 0.9).
       * Reverse=transfrom is just * 0.3.
       ***/
      int virtual_box_size = params->box_size / params->c_bond_natural_length;
      
      // Compact initial conformation of chromatin fiber in [1, virtual_box_size]^3
      std::vector<std::array<int, 3>> virtual_positions;
      std::vector<std::vector<std::vector<bool>>>
        occupied(virtual_box_size,
                 std::vector<std::vector<bool>>(virtual_box_size,
                                                std::vector<bool>(virtual_box_size, false)));
      
      auto mid = virtual_box_size / 2;

      // Seed polymer
      int seed_counter = 0;
      for (int i=1; i < virtual_box_size - 1 && seed_counter < params->num_c_monomers; i++) {
        virtual_positions.push_back({{i, mid, mid}});
        occupied[mid][mid][i] = true;
        seed_counter++;
      }

      for (int i=virtual_box_size - 2; i > 0 && seed_counter < params->num_c_monomers; i--) {
        virtual_positions.push_back({{i, mid + 1, mid}});
        occupied[mid][mid + 1][i] = true;
        seed_counter++;
      }

      // Grow the seed
      for (int i=virtual_positions.size(); i < params->num_c_monomers; i+=2) {
        int loop_counter = 0;
        
        while (true) {
          if (loop_counter++ > 1000000) {
            std::cerr << "Polymer cannot be initialized because of small space." << std::endl;
            std::exit(1);
          }
          
          // Select a growing position
          auto grow_pos = rand_int(0, virtual_positions.size() - 2);
          auto pos1 = virtual_positions[grow_pos];
          auto pos2 = virtual_positions[grow_pos + 1];

          int bond_direction = 0; // 0, 1, or 2 is x, y, or z
          for (int j=0; j<3; j++) {
            if (pos1[j] != pos2[j]) {
              bond_direction = j;
              break;
            }
          }
          
          // Select a growing direction
          int axis = bond_direction;
          while (axis == bond_direction) axis = rand_int(0, 2);
          int sign = rand_int(0, 1) == 0 ? 1 : -1;
          
          std::array<int, 3> new_pos1(pos1);
          std::array<int, 3> new_pos2(pos2);
          new_pos1[axis] = new_pos1[axis] + sign;
          new_pos2[axis] = new_pos2[axis] + sign;

          if (*std::min_element(new_pos1.begin(), new_pos1.end()) > 0 &&
              *std::min_element(new_pos2.begin(), new_pos2.end()) > 0 &&
              *std::max_element(new_pos1.begin(), new_pos1.end()) < virtual_box_size - 1 &&
              *std::max_element(new_pos2.begin(), new_pos2.end()) < virtual_box_size - 1 &&
              !occupied[new_pos1[0]][new_pos1[1]][new_pos1[2]] &&
              !occupied[new_pos2[0]][new_pos2[1]][new_pos2[2]]) {
            virtual_positions.insert(virtual_positions.begin() + grow_pos + 1, {new_pos1, new_pos2});
            occupied[new_pos1[0]][new_pos1[1]][new_pos1[2]] = true;
            occupied[new_pos2[0]][new_pos2[1]][new_pos2[2]] = true;
            break;
          }
        }
      }

      // Reverse-transform
      // If virtual_positions.size() > params->num_c_monomers, remove the monomers at the end of the polymers.
      // (this case happens when params->num_c_monomers is odd number.
      // Because the seed-growning process adds two monomers at one step,
      // this process cannot control the total number of monomers are even or odd.)
      std::vector<OpenMM::Vec3> positions(params->num_c_monomers);
      for (size_t i=0; i < positions.size(); i++) 
        for (int j=0; j < 3; j++) 
          positions[i][j] = virtual_positions[i][j] * params->c_bond_natural_length;

      return positions;
    }


    // Return random positions of X
    // because the nucleation seems to be avoided in the initial positoins.
    std::vector<OpenMM::Vec3> x_random_positions(std::vector<OpenMM::Vec3> polymer_pos)
    {
      // Firstly set the X positions in the [1, virtual_box_size]^3 lattice
      const double scaler = 2 * params->x_radius;
      int virtual_box_size = params->box_size / scaler;
      std::vector<std::array<int, 3>> virtual_positions;
      std::vector<std::vector<std::vector<bool>>>
        occupied(virtual_box_size,
                 std::vector<std::vector<bool>>(virtual_box_size,
                                                std::vector<bool>(virtual_box_size, false)));

      // Occupied by the polymer
      // The occupied region of each monomer is the box
      // whose center is the positoin of the monomer and whose side length is the virtual polymer diamenter.
      auto virtual_c_radius = params->c_radius / scaler;
      for (const auto& pos : polymer_pos) {
        auto vx = pos[0] / scaler;
        auto vy = pos[1] / scaler;
        auto vz = pos[2] / scaler;

        int vx_start = vx - virtual_c_radius; // floor
        int vx_end = std::ceil(vx + virtual_c_radius) + 1;
        int vy_start = vy - virtual_c_radius; // floor
        int vy_end = std::ceil(vy + virtual_c_radius) + 1;
        int vz_start = vz - virtual_c_radius; // floor
        int vz_end = std::ceil(vz + virtual_c_radius) + 1;

        for (int i=vx_start; i<vx_end; i++)
          for (int j=vy_start; j<vy_end; j++)
            for (int k=vz_start; k<vz_end; k++)
              occupied[i][j][k] = true;
      }

      // put X on the lattice randomly
      int loop_counter = 0;
      
      for (int i=0; i < params->num_x; i++) {
        while (true) {
          if (loop_counter++ > 10000) {
            std::cerr << "X cannot be initialized because of small space." << std::endl;
            std::exit(1);
          }

          auto x = rand_int(1, virtual_box_size - 1);
          auto y = rand_int(1, virtual_box_size - 1);
          auto z = rand_int(1, virtual_box_size - 1);

          if (!occupied[x][y][z]) {
            virtual_positions.push_back(std::array<int, 3>{{x, y, z}});
            occupied[x][y][z] = true;
            break;
          }
        }
      }

      // Reverse-transform
      std::vector<OpenMM::Vec3> positions(params->num_x);
      for (size_t i=0; i < positions.size(); i++)
        for (int j=0; j < 3; j++) 
          positions[i][j] = virtual_positions[i][j] * scaler;

      return positions;
    }
    

    // Return the particles positons with the order: polymer postitions -> X positions
    std::vector<OpenMM::Vec3> random_positions()
    {
      // Random compact positions of a chromatin fiber
      auto polymer_pos = polymer_random_positions();
      // Most separated positions of X with equal-sized intervals
      auto x_pos = x_random_positions(polymer_pos);

      // Copy the polymer and X positions with this sequential order
      std::vector<OpenMM::Vec3> positions;
      positions.reserve(polymer_pos.size() + x_pos.size());
      std::copy(polymer_pos.begin(), polymer_pos.end(), std::back_inserter(positions));
      std::copy(x_pos.begin(), x_pos.end(), std::back_inserter(positions));

      return positions;
    }


    // Return the particles positons with the order: polymer postitions -> X positions
    std::vector<OpenMM::Vec3> phased_positions()
    {
      int virtual_box_size;
      int mid;
      std::vector<OpenMM::Vec3> seed_c_positions;
      std::vector<OpenMM::Vec3> x_positions;
      
      /*
       * 1. Set a seed polymer
       */
      // Set a seed polymer in the box [1, virtual_box_size - 2]^3
      std::vector<std::array<int, 3>> seed_virtual_positions;
      virtual_box_size = params->box_size / params->c_bond_natural_length;
      std::vector<std::vector<std::vector<bool>>>
        occupied_for_c(virtual_box_size,
                       std::vector<std::vector<bool>>(virtual_box_size,
                                                      std::vector<bool>(virtual_box_size, false)));
      mid = virtual_box_size / 2;
      int seed_first_margin_length = 0;
      
      // Seed of a margin
      if (mid > params->margin_length) {
        // Whole margin can be set as a seed.
        for (int i=mid - params->margin_length; i <= mid; i++) {
          seed_virtual_positions.push_back({{i, mid, mid}});
          occupied_for_c[i][mid][mid] = true;
          seed_first_margin_length++;
        }

      } else if ((mid % 2 == 0 && params->margin_length % 2 == 0) ||
                 (mid % 2 == 1 && params->margin_length % 2 == 1)) {
        // Because a seed grows 2 monomers at one step,
        // the parity of the seed margin must adjust to the margin lenngth.
        // There is the case where the parities are same.
        for (int i=1; i <= mid; i++) {
          seed_virtual_positions.push_back({{i, mid, mid}});
          occupied_for_c[i][mid][mid] = true;
          seed_first_margin_length++;        
        }

      } else {
        // Adjast the parity; seed margin length is - 1 than above
        for (int i=2; i <= mid; i++) {
          seed_virtual_positions.push_back({{i, mid, mid}});
          occupied_for_c[i][mid][mid] = true;
          seed_first_margin_length++;
        }
      }

      
      // Full-length enhancer
      if (virtual_box_size - 2 - mid < params->e_length) {
        std::cerr << "The box size is too small for enhancer and length."
                  << std::endl;
        std::exit(1);
      }
      
      for (int i=mid + 1; i <= mid + params->e_length; i++) {
        seed_virtual_positions.push_back({{i, mid, mid}});
        occupied_for_c[i][mid][mid] = true;          
      }

      
      // Seed of a polymer between the enancer and the promoter
      const int e_mid = (mid + 1 + mid + params->e_length) / 2;
      int p_start = e_mid + params->p_length / 2;
      const int available_ep_length = virtual_box_size - 1 - (mid + params->e_length + 1)
                               + virtual_box_size - 1 - (p_start + 1);
      const int e_end = mid + params->e_length + 1;
      const int ep_overhang = std::max(e_end, p_start + 1) - std::min(e_end, p_start + 1);
      int seed_ep_length = 0;

      
      if (available_ep_length > params->ep_length) {
        // Full-length E-P polymer can be set
        // because the E-P length is shorter than box size.
        
        // Adjast the parity of ep_length becasue the length of a polymer
        // between the enhancer and the promoter is cotroled by 2 at a time.  
        if (ep_overhang % 2 == params->ep_length % 2) {
          // The parity is correct.
          const int turn_pos = e_end - 1 + (params->ep_length - ep_overhang) / 2;

          for (int i=e_end; i<=turn_pos; i++) {
            seed_virtual_positions.push_back({{i, mid, mid}});
            occupied_for_c[i][mid][mid] = true;
            seed_ep_length++;
          }
          // Turn
          for (int i=turn_pos; i>p_start; i--) {
            seed_virtual_positions.push_back({{i, mid + 1, mid}});
            occupied_for_c[i][mid + 1][mid] = true;
            seed_ep_length++;
          }
        }
        else {
          // The parity is different. Slide the promoter positions.
          p_start -= 1; // must -1 because +1 shorten the available_ep_length.
          int turn_pos;

          if (e_end > p_start + 1) {
            // Enhancer length > promoter length
            // Overhang length becomes longer by sliding promoter.
            turn_pos = e_end - 1 + (params->ep_length - (ep_overhang + 1)) / 2;
          }
          else {
            // Enhancer length <= promoter length
            // Overhang length becomes shorter by sliding promoter.
            turn_pos = e_end - 1 + (params->ep_length - (ep_overhang - 1)) / 2;
          }
          
          for (int i=e_end; i<=turn_pos; i++) {
            seed_virtual_positions.push_back({{i, mid, mid}});
            occupied_for_c[i][mid][mid] = true;
            seed_ep_length++;
          }
          // Turn
          for (int i=turn_pos; i>p_start; i--) {
            seed_virtual_positions.push_back({{i, mid + 1, mid}});
            occupied_for_c[i][mid + 1][mid] = true;
            seed_ep_length++;            
          }
        }
      }
      else {
        // If the full E-P length is equal to seed E-P length,
        // there is no parity problem and p_start will not be changed.
        
        if ((available_ep_length % 2 == 0 && params->ep_length % 2 == 0) ||
            (available_ep_length % 2 == 1 && params->ep_length % 2 == 1)) {
          // Because a seed grows 2 monomers at one step,
          // the parity of the seed length must adjust to the full lenngth.
          // This is the case where the parities are same.
          for (int i=e_end; i<=virtual_box_size - 2; i++) {
            seed_virtual_positions.push_back({{i, mid, mid}});
            occupied_for_c[i][mid][mid] = true;
            seed_ep_length++;
          }
          
          // Turn at the end of the box
          for (int i=virtual_box_size - 2; i>p_start; i--) {
            seed_virtual_positions.push_back({{i, mid + 1, mid}});
            occupied_for_c[i][mid + 1][mid] = true;
            seed_ep_length++;
          }

        }
        else {
          // Adjast the parity by sliding the promoter positions 1
          p_start -= 1; // must -1 because +1 shorten the available_ep_length.

          for (int i=e_end; i<=virtual_box_size - 2; i++) {
            seed_virtual_positions.push_back({{i, mid, mid}});
            occupied_for_c[i][mid][mid] = true;
            seed_ep_length++;
          }
          
          // Turn at the end of the box
          for (int i=virtual_box_size - 2; i>p_start; i--) {
            seed_virtual_positions.push_back({{i, mid + 1, mid}});
            occupied_for_c[i][mid + 1][mid] = true;
            seed_ep_length++;
          }
        }
      }

      
      // Set the full-length promoter
      for (int i=p_start; i > p_start - params->p_length; i--) {
        seed_virtual_positions.push_back({{i, mid + 1, mid}});
        occupied_for_c[i][mid + 1][mid] = true;
      }

      
      // Set the another margin
      int p_end = p_start - params->p_length;
      int seed_second_margin_length = 0;
      
      if (p_end > params->margin_length) {
        // Whole margin can be set as a seed.
        for (int i=p_end; i > p_end - params->margin_length; i--) {
          seed_virtual_positions.push_back({{i, mid + 1, mid}});
          occupied_for_c[i][mid + 1][mid] = true;
          seed_second_margin_length++;
        }

      } else if ((p_end % 2 == 0 && params->margin_length % 2 == 0) ||
                 (p_end % 2 == 1 && params->margin_length % 2 == 1)) {
        // Because a seed grows 2 monomers at one step,
        // the parity of the seed margin must adjust to the margin lenngth.
        // There is the case where the parities are same.
        for (int i=p_end; i > 0; i--) {
          seed_virtual_positions.push_back({{i, mid + 1, mid}});
          occupied_for_c[i][mid + 1][mid] = true;
          seed_second_margin_length++;
        }

      } else {
        // Adjast the parity; seed margin length is - 1 than above
        for (int i=p_end; i > 1; i--) {
          seed_virtual_positions.push_back({{i, mid + 1, mid}});
          occupied_for_c[i][mid + 1][mid] = true;
          seed_second_margin_length++;
        }          
      }

      
      // Reverse transform from the virtual scale to the actual scale
      for (const auto& p : seed_virtual_positions) {
        OpenMM::Vec3 v;
        
        for (int j=0; j < 3; j++) 
          v[j] = p[j] * params->c_bond_natural_length;
        
        seed_c_positions.push_back(v);
      }

      
      /*
       * 2. Set X particles arount the enhancer and the promoter
       */
      // Firstly set the X positions in the [1, virtual_box_size]^3 lattice
      double scaler = 2 * params->x_radius;
      virtual_box_size = params->box_size / scaler;
      std::vector<std::array<int, 3>> x_virtual_positions;
      std::vector<std::vector<std::vector<bool>>>
        occupied_for_x(virtual_box_size,
                 std::vector<std::vector<bool>>(virtual_box_size,
                                                std::vector<bool>(virtual_box_size, false)));
      
      // Occupied by the polymer
      // The occupied region of the each monomer is the box
      // whose center is the positoin of monomer and whose side length is the virtual polymer diamenter.
      int virtual_c_radius = params->c_radius / scaler - 0.5;
      for (const auto& pos : seed_c_positions) {
        auto vx = pos[0] / scaler;
        auto vy = pos[1] / scaler;
        auto vz = pos[2] / scaler;

        int vx_start = vx - virtual_c_radius; // floor
        int vx_end = std::ceil(vx + virtual_c_radius) + 1;
        int vy_start = vy - virtual_c_radius; // floor
        int vy_end = std::ceil(vy + virtual_c_radius) + 1;
        int vz_start = vz - virtual_c_radius; // floor
        int vz_end = std::ceil(vz + virtual_c_radius) + 1;

        for (int i=vx_start; i<vx_end; i++)
          for (int j=vy_start; j<vy_end; j++)
            for (int k=vz_start; k<vz_end; k++)
              occupied_for_x[i][j][k] = true;
      }
      
      
      // Set X on the lattice around the enhancer and the promoter
      // Firstly, set X adjascent to the polymer with this length
      // Then grow more adjascent to these Xs.
      auto e_mid_pos = seed_c_positions[seed_first_margin_length + params->e_length / 2];
      for (auto i : boost::irange(0, 3))
        e_mid_pos[i] /= scaler;
      std::array<int, 3> nucleation_center{{int(e_mid_pos[0]), int(e_mid_pos[1]), int(e_mid_pos[2])}};

      // Fill X particles from the nucleation center to the outside
      int x_add_counter = 0;
      for (int r=0; true; r++) {
        // This loop contain the duplications.
        // The duplicates removed by "occupied" judgement.
        for (auto i : boost::irange(0, 3)) {
          for (auto a : {-r, r}) {
            std::vector<int> face_axis({0, 1, 2});
            face_axis.erase(face_axis.begin() + i); // The coodinate axes of the face
            
            for (auto b : boost::irange(-r, r + 1)) {              
              for (auto c : boost::irange(-r, r + 1)) {
                std::array<int, 3> new_pos = nucleation_center;
                new_pos[i] += a; // Selected a face to fill X particles
                new_pos[face_axis[0]] += b;
                new_pos[face_axis[1]] += c;

                if (new_pos[0] < 1 || new_pos[0] > virtual_box_size - 1 ||
                    new_pos[1] < 1 || new_pos[1] > virtual_box_size - 1 ||
                    new_pos[2] < 1 || new_pos[2] > virtual_box_size - 1) {
                  std::cerr << "Too many X particles to set in the box." << std::endl;
                  std::exit(1);
                }

                auto x = new_pos[0];
                auto y = new_pos[1];
                auto z = new_pos[2];


                if (!occupied_for_x[x][y][z]) {
                  x_virtual_positions.push_back(std::array<int, 3>{{x, y, z}});
                  occupied_for_x[x][y][z] = true;
                  x_add_counter++;
                }
                
                if (x_add_counter >= params->num_x) break;
              }
              if (x_add_counter >= params->num_x) break;
            }
            if (x_add_counter >= params->num_x) break;
          }
          if (x_add_counter >= params->num_x) break;
        }
        if (x_add_counter >= params->num_x) break;
      }

      // Reverse-transform
      if (x_virtual_positions.size() != params->num_x) {
        std::cerr << "The number of X particles is wrong." << std::endl;
        std::exit(1);
      }

      for (const auto& p : x_virtual_positions) {
        OpenMM::Vec3 v;
          
        for (int j=0; j < 3; j++) 
          v[j] = p[j] * scaler;

        x_positions.push_back(v);
      }


      /*
       * 3. Grow the seed polymer
       */
      scaler = params->c_bond_natural_length;
      // Use seed_virtual_positions again
      virtual_box_size = params->box_size / scaler;
      // Use occupied_for_c again
      
      // Calculate the positions Occupied by X particles
      int virtual_x_radius = params->x_radius / scaler - 0.5;
      for (const auto& pos : x_positions) {
        auto vx = pos[0] / scaler;
        auto vy = pos[1] / scaler;
        auto vz = pos[2] / scaler;

        int vx_start = vx - virtual_x_radius; // floor
        int vx_end = std::ceil(vx + virtual_x_radius) + 1;
        int vy_start = vy - virtual_x_radius; // floor
        int vy_end = std::ceil(vy + virtual_x_radius) + 1;
        int vz_start = vz - virtual_x_radius; // floor
        int vz_end = std::ceil(vz + virtual_x_radius) + 1;

        
        for (int i=vx_start; i<vx_end; i++)
          for (int j=vy_start; j<vy_end; j++)
            for (int k=vz_start; k<vz_end; k++)
              occupied_for_c[i][j][k] = true;
      }

      // Grow the seed
      // The two margins and the polymer between the enhancer and the promoter
      // will grow separatedly.
      // Firstly, the margin adjascent to the enhancer
      if (seed_first_margin_length < 2) {
        std::cerr << "The first seed margin are too short to grow." << std::endl;
        std::exit(1);
      }

      int first_margin_length = seed_first_margin_length;
      
      for (int i=seed_first_margin_length; i < params->margin_length; i+=2) {
        int loop_counter = 0;
        
        while (true) {
          if (loop_counter++ > 1000000) {
            std::cerr << "Polymer cannot be initialized because of small space." << std::endl;
            std::exit(1);
          }

          auto grow_pos = rand_int(0, i - 2);
          auto pos1 = seed_virtual_positions[grow_pos];
          auto pos2 = seed_virtual_positions[grow_pos + 1];

          int bond_direction = 0; // 0, 1, or 2 is x, y, or z
          for (int j=0; j<3; j++) {
            if (pos1[j] != pos2[j]) {
              bond_direction = j;
              break;
            }
          }
          
          // Select a growing direction
          int axis = bond_direction;
          while (axis == bond_direction) axis = rand_int(0, 2);
          int sign = rand_int(0, 1) == 0 ? 1 : -1;
          
          std::array<int, 3> new_pos1(pos1);
          std::array<int, 3> new_pos2(pos2);
          new_pos1[axis] = new_pos1[axis] + sign;
          new_pos2[axis] = new_pos2[axis] + sign;

          if (*std::min_element(new_pos1.begin(), new_pos1.end()) > 0 &&
              *std::min_element(new_pos2.begin(), new_pos2.end()) > 0 &&
              *std::max_element(new_pos1.begin(), new_pos1.end()) < virtual_box_size - 1 &&
              *std::max_element(new_pos2.begin(), new_pos2.end()) < virtual_box_size - 1 &&
              !occupied_for_c[new_pos1[0]][new_pos1[1]][new_pos1[2]] &&
              !occupied_for_c[new_pos2[0]][new_pos2[1]][new_pos2[2]]) {
            seed_virtual_positions.insert(seed_virtual_positions.begin() + grow_pos + 1, {new_pos1, new_pos2});
            occupied_for_c[new_pos1[0]][new_pos1[1]][new_pos1[2]] = true;
            occupied_for_c[new_pos2[0]][new_pos2[1]][new_pos2[2]] = true;
            first_margin_length += 2;
            break;
          }
        }
      }

      if (first_margin_length != params->margin_length) {
        std::cerr << "The length of the first margin is wrong." << std::endl;
        std::exit(1);
      }
                                                         
      
      // Secondly, the polymer between the enhancer and the promoter
      if (seed_ep_length < 2) {
        std::cerr << "The seed loop length are too short to grow." << std::endl;
        std::exit(1);
      }

      int ep_length = seed_ep_length;
      int start_idx = first_margin_length + params->e_length;
      
      for (int i=seed_ep_length; i < params->ep_length; i+=2) {
        int loop_counter = 0;
        
        while (true) {
          if (loop_counter++ > 1000000) {
            std::cerr << "Polymer cannot be initialized because of small space." << std::endl;
            std::exit(1);
          }

          auto grow_pos = rand_int(start_idx, start_idx + i - 2);
          auto pos1 = seed_virtual_positions[grow_pos];
          auto pos2 = seed_virtual_positions[grow_pos + 1];

          int bond_direction = 0; // 0, 1, or 2 is x, y, or z
          for (int j=0; j<3; j++) {
            if (pos1[j] != pos2[j]) {
              bond_direction = j;
              break;
            }
          }
          
          // Select a growing direction
          int axis = bond_direction;
          while (axis == bond_direction) axis = rand_int(0, 2);
          int sign = rand_int(0, 1) == 0 ? 1 : -1;
          
          std::array<int, 3> new_pos1(pos1);
          std::array<int, 3> new_pos2(pos2);
          new_pos1[axis] = new_pos1[axis] + sign;
          new_pos2[axis] = new_pos2[axis] + sign;

          if (*std::min_element(new_pos1.begin(), new_pos1.end()) > 0 &&
              *std::min_element(new_pos2.begin(), new_pos2.end()) > 0 &&
              *std::max_element(new_pos1.begin(), new_pos1.end()) < virtual_box_size - 1 &&
              *std::max_element(new_pos2.begin(), new_pos2.end()) < virtual_box_size - 1 &&
              !occupied_for_c[new_pos1[0]][new_pos1[1]][new_pos1[2]] &&
              !occupied_for_c[new_pos2[0]][new_pos2[1]][new_pos2[2]]) {
            seed_virtual_positions.insert(seed_virtual_positions.begin() + grow_pos + 1, {new_pos1, new_pos2});
            occupied_for_c[new_pos1[0]][new_pos1[1]][new_pos1[2]] = true;
            occupied_for_c[new_pos2[0]][new_pos2[1]][new_pos2[2]] = true;
            ep_length += 2;
            break;
          }
        }
      }

      if (ep_length != params->ep_length) {
        std::cerr << "The loop length is wrong." << std::endl;
        std::exit(1);
      }

      
      // Finally, grow the second margin
      if (seed_second_margin_length < 2) {
        std::cerr << "The second seed margin are too short to grow." << std::endl;
        std::exit(1);
      }

      int second_margin_length = seed_second_margin_length;
      start_idx = first_margin_length + params->e_length + ep_length + params->p_length;
      
      for (int i=seed_second_margin_length; i < params->margin_length; i+=2) {
        int loop_counter = 0;
        
        while (true) {
          if (loop_counter++ > 1000000) {
            std::cerr << "Polymer cannot be initialized because of small space." << std::endl;
            std::exit(1);
          }

          auto grow_pos = rand_int(start_idx, start_idx + i - 2);
          auto pos1 = seed_virtual_positions[grow_pos];
          auto pos2 = seed_virtual_positions[grow_pos + 1];

          int bond_direction = 0; // 0, 1, or 2 is x, y, or z
          for (int j=0; j<3; j++) {
            if (pos1[j] != pos2[j]) {
              bond_direction = j;
              break;
            }
          }
          
          // Select a growing direction
          int axis = bond_direction;
          while (axis == bond_direction) axis = rand_int(0, 2);
          int sign = rand_int(0, 1) == 0 ? 1 : -1;
          
          std::array<int, 3> new_pos1(pos1);
          std::array<int, 3> new_pos2(pos2);
          new_pos1[axis] = new_pos1[axis] + sign;
          new_pos2[axis] = new_pos2[axis] + sign;

          if (*std::min_element(new_pos1.begin(), new_pos1.end()) > 0 &&
              *std::min_element(new_pos2.begin(), new_pos2.end()) > 0 &&
              *std::max_element(new_pos1.begin(), new_pos1.end()) < virtual_box_size - 1 &&
              *std::max_element(new_pos2.begin(), new_pos2.end()) < virtual_box_size - 1 &&
              !occupied_for_c[new_pos1[0]][new_pos1[1]][new_pos1[2]] &&
              !occupied_for_c[new_pos2[0]][new_pos2[1]][new_pos2[2]]) {
            seed_virtual_positions.insert(seed_virtual_positions.begin() + grow_pos + 1, {new_pos1, new_pos2});
            occupied_for_c[new_pos1[0]][new_pos1[1]][new_pos1[2]] = true;
            occupied_for_c[new_pos2[0]][new_pos2[1]][new_pos2[2]] = true;
            second_margin_length += 2;
            break;
          }
        }
      }

      if (second_margin_length != params->margin_length) {
        std::cerr << "The length of the second margin is wrong." << std::endl;
        std::exit(1);
      }

      
      // Reverse transform to the actual scale
      if (seed_virtual_positions.size() != params->num_c_monomers) {
        std::cerr << "The polymer length is wrong." << std::endl;
        std::exit(1);
      }

      std::vector<OpenMM::Vec3> c_positions;
      
      for (const auto& p : seed_virtual_positions) {
        OpenMM::Vec3 v;
        
        for (int j=0; j < 3; j++) 
          v[j] = p[j] * params->c_bond_natural_length;
        
        c_positions.push_back(v);
      }


      // Copy the polymer and X positions with this sequential order
      std::vector<OpenMM::Vec3> positions;
      positions.reserve(c_positions.size() + x_positions.size());
      std::copy(c_positions.begin(), c_positions.end(), std::back_inserter(positions));
      std::copy(x_positions.begin(), x_positions.end(), std::back_inserter(positions));

      if (positions.size() != params->num_c_monomers + params->num_x) {
        std::cerr << "The final number of the particles is wrong." << std::endl;
        std::exit(1);
      }

      return positions;
    }

    
    // Sampled from Maxwell distribution
    std::vector<OpenMM::Vec3> random_velocities()
    {
      std::vector<OpenMM::Vec3> velocities;
      std::random_device rnd;
      std::mt19937 mt(rnd());

      std::vector<double> masses{params->c_mass, params->x_mass};

      std::vector<double> std_devs(masses.size());
      for (size_t i = 0; i < masses.size(); i++)
        std_devs[i] = sqrt(params->kB * params->temperature / masses[i]) * 1e-3; // [nm / ps]

      std::vector<std::normal_distribution<double>> normals;
      for (size_t i = 0; i < masses.size(); i++)
        normals.push_back(std::normal_distribution<double>(0, std_devs[i]));

      std::vector<int> nums = {params->num_c_monomers, params->num_x};
      for (size_t i=0; i < masses.size(); i++)
        for (int j=0; j < nums[i]; j++)
          velocities.push_back(OpenMM::Vec3(normals[i](mt), normals[i](mt), normals[i](mt)));
      
      return velocities;
    }

    
  public:
    ~Simulator_3D()
    {
      delete context;
      delete system;
      delete platform;
      delete integrator;
    }

    
    Simulator_3D(const std::shared_ptr<const EPSim::Parameters>& p)
      : params(p)
    {
      
      // System
      system = new OpenMM::System();
      system->setDefaultPeriodicBoxVectors({p->box_size, 0, 0},
                                           {0, p->box_size, 0},
                                           {0, 0, p->box_size});

      int particle_counter = 0;
      for (int i=0; i < p->num_c_monomers; i++) {
        system->addParticle(p->c_mass);

        if (std::find(p->e_positions.begin(), p->e_positions.end(), i) != p->e_positions.end())
          e_idx.push_back(particle_counter); // Enhancer
        else if (std::find(p->p_positions.begin(), p->p_positions.end(), i) != p->p_positions.end())
          p_idx.push_back(particle_counter); // Promoter
        else
          c_idx.push_back(particle_counter); // Othe chromatin particles

        particle_counter++;
      }

      for (int i=0; i < p->num_x; i++) {
        system->addParticle(p->x_mass);
        x_idx.push_back(particle_counter);
        particle_counter++;
      }

      // Forces
      // Bonds of chromatin fiber
      bond_force = new OpenMM::HarmonicBondForce();
      for (int i=1; i < p->num_c_monomers; i++)
        bond_force->addBond(i - 1, i, p->c_bond_natural_length, p->c_bond_k);
      system->addForce(bond_force);

      // Stiffness (Bending forces)
      stiff_force = new OpenMM::HarmonicAngleForce();
      for (int i=2; i < p->num_c_monomers; i++)
        stiff_force->addAngle(i - 2, i - 1, i, p->c_natural_angle, p->c_angle_k);
      system->addForce(stiff_force);

      // Nonbonded attractive and repulsive forces (LJ potensial)
      const std::string lj_force_eq = "4 * epsilon * ((sigma/r)^12 - (sigma/r)^6);";

      std::vector<std::vector<int>> idx{c_idx, e_idx, p_idx, x_idx};
      int force_group_counter = 1;

      // Set LJ force for all pairs of the particle groups: C, E, P, X
      // They can be defined by using force groups.
      for (size_t i=0; i < idx.size(); i++) {
        auto idx1 = std::set<int>(idx[i].begin(), idx[i].end());
                    
        for (size_t j=i; j < idx.size(); j++) {
          auto idx2 = std::set<int>(idx[j].begin(), idx[j].end());
          
          OpenMM::CustomNonbondedForce* lj_force = new OpenMM::CustomNonbondedForce(lj_force_eq);
          lj_force->addGlobalParameter("sigma", p->lj_sigma_table[i][j]);
          lj_force->addGlobalParameter("epsilon", p->lj_epsilon_table[i][j]);
          lj_force->setNonbondedMethod(lj_force->CutoffPeriodic); // Periodic boundary condition
          lj_force->setCutoffDistance(p->lj_cutoff_table[i][j]);
          lj_force->setUseSwitchingFunction(true);
          lj_force->setSwitchingDistance(p->lj_switch_table[i][j]);

          for (int k=0; k < system->getNumParticles(); k++)
            lj_force->addParticle();
          
          // Restrict the force to idx1 and idx2
          lj_force->addInteractionGroup(idx1, idx2);

          // LJ potential is not applied to every two adjascent chromatin monomers
          if (i == 0 && j == 0)
            for (int k=1; k < p->num_c_monomers; k++)
              lj_force->addExclusion(k - 1, k);

          lj_force->setForceGroup(force_group_counter++);
          lj_forces.push_back(lj_force);
          system->addForce(lj_force);
        }
      }
      
      // Platform
      OpenMM::Platform::loadPluginsFromDirectory(params->plugin_dir);

      // Accept CUDA and CPU
      auto platform_name_lower = p->platform_name;
      std::transform(platform_name_lower.begin(), platform_name_lower.end(),
                     platform_name_lower.begin(), ::tolower);
      
      if (platform_name_lower == "cuda") {
        platform = &OpenMM::Platform::getPlatformByName("CUDA");
        platform->setPropertyDefaultValue("CudaPrecision", p->precision);
      }
      else if (platform_name_lower == "cpu") {
        platform = &OpenMM::Platform::getPlatformByName("CPU");
      }
      else {
        std::cerr << "Error: Accepting only CUDA or CPU" << std::endl;
        std::exit(1);
      }

      
      // Integrator
      auto integrator_name_lower = p->integrator_name;
      std::transform(integrator_name_lower.begin(), integrator_name_lower.end(),
                     integrator_name_lower.begin(), ::tolower);

      // Accept only langevin
      if (integrator_name_lower == "langevin") {
        integrator = new OpenMM::LangevinIntegrator(p->temperature, p->friction_coeff,
                                                    p->time_step);
      }
      else {
        std::cerr << "Error: Accepting only langeivn" << std::endl;
        std::exit(1);
      }
    }


    void init()
    {
      context = new OpenMM::Context(*system, *integrator, *platform);
      //context->setPositions(random_positions());
      context->setPositions(phased_positions());
      OpenMM::LocalEnergyMinimizer lem;
      lem.minimize(*context);
      context->setVelocities(random_velocities());

      auto state = get_state();
      std::cout << "Initial kinetics: " <<
        state.getKineticEnergy() / (params->num_c_monomers + params->num_x) / params->kBT_to_kJPerMol <<
        std::endl;
      std::cout << "Initail potential: " <<
        state.getPotentialEnergy() / (params->num_c_monomers + params->num_x) / params->kBT_to_kJPerMol <<
        std::endl;
    }

    
  public:
    void steps(int num_steps)
    {
      // No local energy minimization
      integrator->step(num_steps);
    }

    OpenMM::State get_state() const
    {
      return context->getState(OpenMM::State::Positions |
                               OpenMM::State::Velocities |
                               OpenMM::State::Forces |
                               OpenMM::State::Energy);
    }

    
    void print_energy()
    {
      auto state = get_state();
      std::cout << std::setw(5) << std::left << std::setfill('0') <<
        "Kinetics: " <<
        state.getKineticEnergy() / (params->num_c_monomers + params->num_x) / params->kBT_to_kJPerMol <<
        "    Potential: " <<
        state.getPotentialEnergy() / (params->num_c_monomers + params->num_x) / params->kBT_to_kJPerMol;
    }
    
    
    std::vector<OpenMM::Vec3> get_positions() const
    {
      return context->getState(OpenMM::State::Positions, true).getPositions();
    }


    void save_positions(H5::H5File file, std::string data_name) const
    {
      auto positions = get_positions();
      
      // Create data set
      auto nrow = positions.size();
      auto ncol = 3;
      hsize_t data_dim[2]{hsize_t(nrow), hsize_t(ncol)};
      H5::DataSpace data_space(2, data_dim);
      auto data_set = file.createDataSet(data_name.c_str(), H5::PredType::NATIVE_DOUBLE, data_space);
      
      // For HDF5, the array must have a contiguous memory.
      // So, it cannot recognize a pointer array, e.g. std::vector and double**.
      double *data = new double[nrow * ncol];
      
      for (size_t i = 0; i < positions.size(); i++) {
        for (int j = 0; j < 3; j++) {
          data[i * ncol + j] = positions[i][j];
        }
      }

      data_set.write(data, H5::PredType::NATIVE_DOUBLE);
      delete data;
    }
  };

    class EP_simulator
  {
    const std::shared_ptr<const EPSim::Parameters> params;
    EPSim::Simulator_3D sim_3d;


    
  public:
    EP_simulator(const std::shared_ptr<const EPSim::Parameters> p)
      : params(p), sim_3d(p) {}
    
    void run()
    {
      H5::H5File h5_file(params->out_file.c_str(), H5F_ACC_TRUNC);

      // Initialization
      std::cout << "Initialization" << std::endl;
      sim_3d.init();

      // Relaxation
      std::cout << "Relaxation" << std::endl;
      for (int i=0; i < params->relax_cycles; i++) {
        sim_3d.steps(params->relax_steps_per_cycle);
        
        // Print the progress and energy
        std::cout << "Relaxation: " << (i + 1) * params->relax_steps_per_cycle << "/"
                  << params->relax_cycles * params->relax_steps_per_cycle << "    ";
        sim_3d.print_energy();
        std::cout << std::endl;
      }

      // Run until reaching total saved steps      
      int saved_steps_counter = 0;
      bool break_flag = false;
      
      while (saved_steps_counter < params->total_saved_steps) {
        for (int i=0; i < params->saved_steps; i++) {
          sim_3d.steps(1);
          sim_3d.save_positions(h5_file, std::to_string(saved_steps_counter));
          saved_steps_counter++;

          if (saved_steps_counter >= params->total_saved_steps) {
            break_flag = true;
            break;
          }
        }

        std::cout << "Saved: " << saved_steps_counter << "/"
                  << params->total_saved_steps << "    ";
        sim_3d.print_energy();
        std::cout << std::endl;

        if (!break_flag)
          sim_3d.steps(params->non_saved_steps);
      }
    }
  };
}


int main(int argc, char *argv[])
{
  const std::string out_file = argv[1];
  const std::string plugin_dir = argv[2];
  const int num_x = std::atoi(argv[3]);
  const double epsilon = std::atof(argv[4]);
  const double epsilon2 = std::atof(argv[5]);
  const int loop_length = std::atoi(argv[6]);

  const std::shared_ptr<const EPSim::Parameters> params(new EPSim::Parameters(out_file, plugin_dir, num_x,
                                                                              epsilon, epsilon2, loop_length));
  const std::unique_ptr<EPSim::EP_simulator> sim(new EPSim::EP_simulator(params));
  sim->run();
  
  return 0;
}


