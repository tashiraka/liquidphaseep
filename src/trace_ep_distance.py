#!/home/yuichi/bin/python2.7

import sys, os, h5py
import numpy as np
import matplotlib.pyplot as plt

args = sys.argv
in_file = args[1]
sample_num = int(args[2])
ep_length = int(args[3])
out_file = args[4]

sample_start = 0
sample_end = sample_num

margin_length = 100
p_length = 1
#ep_length = 67
e_length = 15

# Min distance between an enhancer and a promoter

e_idxes = range(margin_length,
                margin_length + e_length)
p_idxes = [margin_length + e_length + ep_length]


h5file = h5py.File(in_file, "r")
ep_dist_traj = []

for i in range(sample_start, sample_end):
    positions = h5file[str(i)].value

    e_pos = [positions[i] for i in e_idxes]
    p_pos = [positions[i] for i in p_idxes]


    min_dist = np.inf
    for e_p in e_pos:
        for p_p in p_pos:
            min_dist = np.min([min_dist, np.linalg.norm(np.array(e_p) - np.array(p_p))])
            
    ep_dist_traj += [min_dist]

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(range(len(ep_dist_traj)), ep_dist_traj)
plt.tight_layout()
plt.show()
plt.savefig(out_file)
plt.close()
