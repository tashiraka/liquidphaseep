#!/home/yuichi/bin/python2.7

import sys, os, h5py
import numpy as np
from sklearn.neighbors import radius_neighbors_graph
from scipy.sparse import csgraph
import matplotlib.pyplot as plt

args = sys.argv
out_file = '/work2/yuichi/Project_EP/x_phase_diagram/x_phase_diagram_contour.eps'

concentrations = np.arange(1, 51, 1) * 0.01 # equally sized bin
epsilons = np.arange(10, 510, 10) # equally sized bin

data_path = '/work2/yuichi/Project_EP/x_phase_diagram'


# x: concentrations, y: epsilons 
diagram = np.empty((len(epsilons), len(concentrations))) * np.nan;


for x_idx, concentration in enumerate(concentrations):
    for y_idx, epsilon in enumerate(epsilons):
        hdf_name = data_path + '/for_x_phase_diagram_' + ('%f' % concentration).rstrip('0') + '_' +  str(epsilon) + '.hdf5'
        
        h5file = h5py.File(hdf_name, 'r')
        X = h5file['0'].value
        n = len(X)

        # 1. Construct a nearest neighbor graph from X positions.
        r = 4 # two times of x diameter
        A = radius_neighbors_graph(X, r, include_self=True, n_jobs=-1)

        # 2. Measure the number of X particles in the larger connected componet of the graph.
        n_components, labels = csgraph.connected_components(A, directed=False)
        largest_cc_size = np.max(np.bincount(labels))

        diagram[len(epsilons) - 1 - y_idx][x_idx] = largest_cc_size / float(n)
        # 3. Repeat 1 and 2 for the all samples.

# 4. Draw the heat map of those numbers with the axies of X concentration and energy coefficient epsilon.
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x, y = np.meshgrid(concentrations, epsilons)
im = ax.contourf(x, y, diagram)
cbar = plt.colorbar(mappable=im)
ax.set_xlim([concentrations.min(), concentrations.max()])
ax.set_ylim([epsilons.min(), epsilons.max()])
ax.set_xticks(np.arange(0, 51, 10) * 0.01)
ax.set_yticks(np.arange(0, 510, 100))
ax.set_xlabel('Concentration')
ax.set_ylabel('Epsilon')
ax.set_title('The size of the largenst droplet of X particles')
plt.draw()
plt.savefig(out_file, format='eps', bbox_inches="tight", pad_inches=0.2)
plt.close()

