#!/bin/bash

#
# Dir pathes
#
data_path=/work2/yuichi/Project_EP/x_phase_diagram
plugin_dir=../externals/OpenMM/installed/lib/plugins

if [ ! -d $data_path ]; then
    mkdir $data_path
fi
            
# Parameters
box_size=50
concentrations=`seq -f%g 0.01 0.01 0.5`
epsilons=(0) #`seq -f%g 0 10 500`

for concentration in ${concentrations[@]}; do
    for epsilon in ${epsilons[@]}; do
        id=${concentration}_${epsilon}        
        hdf=${data_path}/for_x_phase_diagram_${id}.hdf5
        
        set -x
        time LD_LIBRARY_PATH=../externals/OpenMM/installed/lib:../externals/OpenMM/installed/lib/plugins:../externals/HDF5/installed/lib:$LD_LIBRARY_PATH ./simulate_x_only $hdf $plugin_dir $box_size $concentration $epsilon
        set +x
    done
done
