#!/bin/bash

if [ ! -d png ]; then
    mkdir png
fi


sample_num=1000


epsilons=(100)
epsilon2s=(100)
loop_lengthes=(500)


for epsilon in ${epsilons[@]}; do
    for epsilon2 in ${epsilon2s[@]}; do
        for loop_length in ${loop_lengthes[@]}; do
            
            hdf=test_${epsilon}_${epsilon2}_${loop_length}.hdf5            
            png=test_${epsilon}_${epsilon2}_${loop_length}.png
            
            python trace_ep_distance.py $hdf $sample_num $loop_length $png 
        done
    done
done
