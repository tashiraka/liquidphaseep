SRC_DIR=./src
TEST_DIR=./test


.PHONY: all main test externals clean

all: externals main 

main:
	make -C src

externals:
	make -C externals

clean:
	make -C src clean
	make -C externals clean
